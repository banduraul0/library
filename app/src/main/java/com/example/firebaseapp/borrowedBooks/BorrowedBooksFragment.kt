package com.example.firebaseapp.borrowedBooks

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import com.example.firebaseapp.R
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapp.dataClasses.Book
import com.example.firebaseapp.dataClasses.Comment
import com.example.firebaseapp.dataClasses.User

lateinit var studentUsername: String

class BorrowedBooksFragment : Fragment(), BorrowedBooksAdapter.BorrowedBooksListClickListener, LifecycleObserver {
    private var booksRecyclerView: RecyclerView? = null
    private lateinit var viewModel: BorrowedBooksViewModel
    private var noBorrowedBooksTextView: TextView? = null
    private lateinit var bookListObserver: Observer<ArrayList<Book>>
    private lateinit var commentObserver: Observer<Boolean>
    private lateinit var commentErrorObserver: Observer<String>
    private lateinit var studentObserver: Observer<User>
    private var adapter: BorrowedBooksAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(BorrowedBooksViewModel::class.java)

        val view = inflater.inflate(R.layout.fragment_borrowed_books, container, false)

        booksRecyclerView = view.findViewById(R.id.books_recyclerview)
        booksRecyclerView?.layoutManager = LinearLayoutManager(requireActivity())
        adapter = BorrowedBooksAdapter(this)
        booksRecyclerView?.adapter = adapter

        noBorrowedBooksTextView = view.findViewById(R.id.noBorrowedBooksTextView)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        booksRecyclerView = null
        noBorrowedBooksTextView = null
        adapter = null
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun removeObservers() {
        viewModel.bookListLiveData.removeObserver(bookListObserver)
        viewModel.commentLiveData.removeObserver(commentObserver)
        viewModel.commentErrorLiveData.removeObserver(commentErrorObserver)
        viewModel.studentLiveData.removeObserver(studentObserver)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun initObservers() {
        bookListObserver = Observer {
            adapter?.setData(it)
            if (it.isEmpty()) {
                noBorrowedBooksTextView?.visibility = View.VISIBLE
            } else {
                noBorrowedBooksTextView?.visibility = View.INVISIBLE
            }
        }
        commentObserver = Observer {
            if (it) {
                Toast.makeText(requireActivity(), "Comment added!", Toast.LENGTH_SHORT).show()
            }
        }
        commentErrorObserver = Observer {
            Toast.makeText(requireActivity(), viewModel.commentErrorLiveData.value!!, Toast.LENGTH_SHORT).show()
        }
        studentObserver = Observer { user -> studentUsername = user.username!! }

        viewModel.bookListLiveData.observe(this, bookListObserver)
        viewModel.commentLiveData.observe(this, commentObserver)
        viewModel.commentErrorLiveData.observe(this, commentErrorObserver)
        viewModel.studentLiveData.observe(this, studentObserver)
    }

    override fun borrowedBookItemClicked(book: Book) {
        val commentEditText = EditText(requireActivity())
        commentEditText.inputType = InputType.TYPE_CLASS_TEXT
        val alertDialog = AlertDialog.Builder(requireActivity())
            .setTitle("What do you think about ${book.title} by ${book.author}?")
            .setView(commentEditText)
            .setPositiveButton(R.string.add_comment) { _, _ -> }.create()

        alertDialog.show()
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            if (commentEditText.text.toString().trim() != "") {
                val comment = Comment(studentUsername, commentEditText.text.toString().trim())
                viewModel.addComment(book, comment)
                alertDialog.dismiss()
            } else {
                Toast.makeText(requireActivity(), "The comment can't be empty!", Toast.LENGTH_SHORT).show()
            }
        }
    }
}