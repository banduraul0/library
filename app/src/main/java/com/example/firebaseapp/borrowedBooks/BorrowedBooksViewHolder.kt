package com.example.firebaseapp.borrowedBooks

import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapp.R

class BorrowedBooksViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    var bookInfoTextView: TextView = itemView.findViewById(R.id.bookString)
    var returnBookBtn: Button = itemView.findViewById(R.id.buttonReturnBook)
}