package com.example.firebaseapp.borrowedBooks

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapp.R
import com.example.firebaseapp.dataClasses.Book
import com.example.firebaseapp.dataClasses.User
import com.google.firebase.firestore.FirebaseFirestore

class BorrowedBooksAdapter(private val clickListener: BorrowedBooksListClickListener): RecyclerView.Adapter<BorrowedBooksViewHolder>() {
    private val books: ArrayList<Book> = ArrayList()

    interface BorrowedBooksListClickListener {
        fun borrowedBookItemClicked(book: Book)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BorrowedBooksViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.books_view_holder, parent, false)
        return BorrowedBooksViewHolder(view)
    }

    override fun getItemCount(): Int {
        return books.size
    }

    override fun onBindViewHolder(holder: BorrowedBooksViewHolder, position: Int) {
        val text = "${books[position].title} by ${books[position].author}"
        holder.bookInfoTextView.text = text
        holder.returnBookBtn.setOnClickListener {
            val firestore = FirebaseFirestore.getInstance()
            firestore.collection("books").document(books[position].title!!).update("borrowed", false)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        firestore.collection("users").whereEqualTo("username", studentUsername)
                            .get().addOnSuccessListener { documents ->
                                for (doc in documents) {
                                    val user = doc.toObject(User::class.java)
                                    user.books?.removeAt(position)
                                    firestore.collection("users").document(doc.id).update("books", user.books!!).addOnCompleteListener { updateTask ->
                                        if (updateTask.isSuccessful) {
                                            Toast.makeText(it.context.applicationContext, "You have returned ${text}!", Toast.LENGTH_SHORT).show()
                                        }
                                    }
                                }
                            }
                    }
                }
        }
        holder.itemView.setOnClickListener { clickListener.borrowedBookItemClicked(books[position]) }
    }

    fun setData(bookList: ArrayList<Book>) {
        books.clear()
        books.addAll(bookList)
        notifyDataSetChanged()
    }
}