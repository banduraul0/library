package com.example.firebaseapp.borrowedBooks

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.firebaseapp.dataClasses.Book
import com.example.firebaseapp.dataClasses.Comment
import com.example.firebaseapp.dataClasses.ListOfComments
import com.example.firebaseapp.dataClasses.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore

class BorrowedBooksViewModel: ViewModel() {
    private lateinit var firestore: FirebaseFirestore
    val bookListLiveData = MutableLiveData<ArrayList<Book>>(ArrayList())
    val commentLiveData = MutableLiveData<Boolean>()
    val commentErrorLiveData = MutableLiveData<String>()
    val studentLiveData = MutableLiveData<User>()

    private fun registerBorrowedBookLiveListener() {
        firestore = FirebaseFirestore.getInstance()
        val userId = FirebaseAuth.getInstance().currentUser!!.uid
        firestore.collection("users").document(userId).get().addOnSuccessListener { documentSnapshot ->
            val user = documentSnapshot.toObject(User::class.java)!!
            studentLiveData.postValue(user)
            firestore.collection("users").addSnapshotListener { value, error ->
                if (error != null) {
                    Log.w("BORROWED_BOOK_DATA_ERR", error.localizedMessage!!)
                }
                value?.let {
                    val borrowedBookList = ArrayList<Book>()
                    for (dc in it.documentChanges) {
                        when (dc.type) {
                            DocumentChange.Type.ADDED -> {
                                val user1 = dc.document.toObject(User::class.java)
                                user1.username?.let { username ->
                                    if (username == user.username!!) {
                                        user1.books?.let { books ->
                                            borrowedBookList.addAll(books)
                                        }
                                    }
                                }
                            }
                            DocumentChange.Type.MODIFIED -> {
                                val user1 = dc.document.toObject(User::class.java)
                                user1.username?.let { username ->
                                    if (username == user.username!!) {
                                        user1.books?.let { books ->
                                            borrowedBookList.addAll(books)
                                        }
                                    }
                                }
                            }
                            DocumentChange.Type.REMOVED -> Log.d("BOOK_REMOVED", dc.document.toString())
                        }
                    }
                    bookListLiveData.postValue(borrowedBookList)
                }
            }
        }
    }

    fun addComment(book: Book, comment: Comment) {
        firestore = FirebaseFirestore.getInstance()

        firestore.collection("comments").document(book.title!!).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                task.result?.let { documentSnapshot ->
                    val comments = documentSnapshot.toObject(ListOfComments::class.java)
                    comments?.let { listOfComments ->
                        listOfComments.comments.add(comment)
                        firestore.collection("comments").document(book.title).update("comments", listOfComments.comments).addOnCompleteListener { querySnapshot ->
                            if (querySnapshot.isSuccessful) {
                                commentLiveData.postValue(true)
                            } else {
                                commentLiveData.postValue(false)
                                querySnapshot.exception?.let { commentErrorLiveData.postValue(it.localizedMessage) }
                            }
                        }
                    }
                }
            } else {
                commentLiveData.postValue(false)
                task.exception?.let { commentErrorLiveData.postValue(it.localizedMessage) }
            }
        }
    }

    init {
        registerBorrowedBookLiveListener()
    }
}