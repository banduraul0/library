package com.example.firebaseapp.showStudents

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapp.R
import com.example.firebaseapp.dataClasses.User

class StudentsAdapter : RecyclerView.Adapter<StudentsViewHolder>() {
    private val students: ArrayList<User> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.students_view_holder, parent, false)
        return StudentsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return students.size
    }

    override fun onBindViewHolder(holder: StudentsViewHolder, position: Int) {
        holder.studentPositionTextView.text = (position + 1).toString()
        holder.studentInfoTextView.text = students[position].username
    }

    fun setData(studentList: ArrayList<User>) {
        students.clear()
        students.addAll(studentList)
        notifyDataSetChanged()
    }
}