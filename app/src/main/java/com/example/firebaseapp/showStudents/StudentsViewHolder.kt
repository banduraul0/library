package com.example.firebaseapp.showStudents

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapp.R

class StudentsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    var studentPositionTextView: TextView = itemView.findViewById(R.id.studentNumber)
    var studentInfoTextView: TextView = itemView.findViewById(R.id.studentString)
}