package com.example.firebaseapp.showStudents

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapp.R
import com.example.firebaseapp.dataClasses.User

class ShowStudentsFragment : Fragment(), LifecycleObserver {
    private var studentsRecyclerView: RecyclerView? = null
    private var noStudentsTextView: TextView? = null
    private lateinit var viewModel: StudentsViewModel
    private lateinit var studentListObserver: Observer<ArrayList<User>>
    private var adapter: StudentsAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(StudentsViewModel::class.java)

        val view = inflater.inflate(R.layout.fragment_show_students, container, false)

        studentsRecyclerView = view.findViewById(R.id.students_recyclerview)
        studentsRecyclerView?.layoutManager = LinearLayoutManager(requireActivity())
        adapter = StudentsAdapter()
        studentsRecyclerView?.adapter = adapter

        noStudentsTextView = view.findViewById(R.id.noStudentsTextView)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        studentsRecyclerView = null
        noStudentsTextView = null
        adapter = null
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun removeObservers() {
        viewModel.studentListLiveData.removeObserver(studentListObserver)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun initObservers() {
        studentListObserver = Observer {
            adapter?.setData(it)
            if (it.isEmpty()) {
                noStudentsTextView?.visibility = View.VISIBLE
            } else {
                noStudentsTextView?.visibility = View.INVISIBLE
            }
        }

        viewModel.studentListLiveData.observe(this, studentListObserver)
    }
}