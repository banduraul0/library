package com.example.firebaseapp.showStudents

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.firebaseapp.dataClasses.User
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.FirebaseFirestore

class StudentsViewModel: ViewModel() {
    private lateinit var firestore: FirebaseFirestore
    val studentListLiveData = MutableLiveData<ArrayList<User>>(ArrayList())

    private fun registerStudentLiveListener() {
        firestore = FirebaseFirestore.getInstance()
        firestore.collection("users").addSnapshotListener { value, error ->
            if (error != null) {
                Log.w("USERS_DATA_ERROR", error.localizedMessage!!)
            }
            value?.let { querySnapshot ->
                val studentList = ArrayList<User>()
                studentList.addAll(studentListLiveData.value!!)
                for (dc in querySnapshot.documentChanges) {
                    when (dc.type) {
                        DocumentChange.Type.ADDED -> {
                            val user = dc.document.toObject(User::class.java)
                            user.student?.let {
                                if (it) {
                                    studentList.add(user)
                                }
                            }
                        }
                        DocumentChange.Type.MODIFIED -> Log.d("USER_MODIFIED", dc.document.toObject(User::class.java).toString())
                        DocumentChange.Type.REMOVED -> Log.d("USER_REMOVED", dc.document.toObject(User::class.java).toString())
                    }
                }
                studentListLiveData.postValue(studentList)
            }
        }
    }

    init {
        registerStudentLiveListener()
    }
}