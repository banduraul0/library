package com.example.firebaseapp.addBook

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import com.example.firebaseapp.R
import com.example.firebaseapp.databinding.FragmentAddBookBinding

class AddBookFragment : Fragment(), LifecycleObserver {
    private lateinit var viewModel: AddBookViewModel
    private var binding: FragmentAddBookBinding? = null
    private lateinit var bookTitleObserver: Observer<String>
    private lateinit var bookAuthorObserver: Observer<String>
    private lateinit var statusObserver: Observer<Boolean>
    private lateinit var errorObserver: Observer<String>
    private var bookTitleFlag = false
    private var bookAuthorFlag = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(AddBookViewModel::class.java)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_book, container, false)
        binding!!.viewModel = viewModel

        val addBookBtn: Button = binding!!.root.findViewById(R.id.buttonAddBook)
        addBookBtn.setOnClickListener { onAddBookClick() }
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun removeObservers() {
        viewModel.bookTitleLiveData.removeObserver(bookTitleObserver)
        viewModel.bookAuthorLiveData.removeObserver(bookAuthorObserver)
        viewModel.responseStatusLiveData.removeObserver(statusObserver)
        viewModel.responseErrorMessageLiveData.removeObserver(errorObserver)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun initObservers() {
        bookTitleObserver = Observer { bookTitleFlag = it != "" }
        bookAuthorObserver = Observer { bookAuthorFlag = it != "" }
        statusObserver = Observer {
            if (it) {
                Toast.makeText(requireActivity(), "The book was successfully added!", Toast.LENGTH_SHORT).show()
                binding!!.bookTitleEditText.text.clear()
                binding!!.bookAuthorEditText.text.clear()
                binding!!.bookAuthorEditText.clearFocus()
            }
        }
        errorObserver = Observer { Toast.makeText(requireActivity(), viewModel.responseErrorMessageLiveData.value!!, Toast.LENGTH_SHORT).show() }

        viewModel.bookTitleLiveData.observe(this, bookTitleObserver)
        viewModel.bookAuthorLiveData.observe(this, bookAuthorObserver)
        viewModel.responseStatusLiveData.observe(this, statusObserver)
        viewModel.responseErrorMessageLiveData.observe(this, errorObserver)
    }

    private fun onAddBookClick() {
        if (bookTitleFlag && bookAuthorFlag) {
            viewModel.addNewBook()
        } else {
            Toast.makeText(requireActivity(), "All fields are mandatory!", Toast.LENGTH_SHORT).show()
        }
    }
}