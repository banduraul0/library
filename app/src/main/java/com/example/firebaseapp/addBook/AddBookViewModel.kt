package com.example.firebaseapp.addBook

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.firebaseapp.dataClasses.Book
import com.example.firebaseapp.dataClasses.ListOfComments
import com.google.firebase.firestore.FirebaseFirestore

class AddBookViewModel: ViewModel() {
    val bookTitleLiveData = MutableLiveData<String>()
    val bookAuthorLiveData = MutableLiveData<String>()
    val responseStatusLiveData = MutableLiveData<Boolean>()
    val responseErrorMessageLiveData = MutableLiveData<String>()

    private val firestore: FirebaseFirestore = FirebaseFirestore.getInstance()

    fun addNewBook() {
        firestore.collection("books").whereEqualTo("title", bookTitleLiveData.value!!.trim())
            .get().addOnSuccessListener { querySnapshot ->
                if (querySnapshot.isEmpty) {
                    val book = Book(
                        bookTitleLiveData.value!!.trim(),
                        bookAuthorLiveData.value!!.trim()
                    )
                    firestore.runTransaction { transaction ->
                        val docRef = firestore.collection("books").document(book.title!!)
                        transaction.set(docRef, book)

                        val comRef = firestore.collection("comments").document(book.title)
                        transaction.set(comRef, ListOfComments())
                    }.addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            responseStatusLiveData.postValue(true)
                        } else {
                            responseStatusLiveData.postValue(false)
                            task.exception?.let { responseErrorMessageLiveData.postValue(it.localizedMessage) }
                        }
                    }
                } else {
                    responseStatusLiveData.postValue(false)
                    responseErrorMessageLiveData.postValue("A book with this title already exists!")
                }
            }
    }
}