package com.example.firebaseapp.logIn

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.firebaseapp.dataClasses.User
import com.example.firebaseapp.newAccount.encrypt
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class LoginViewModel: ViewModel() {
    val emailLiveData = MutableLiveData<String>()
    val passwordLiveData = MutableLiveData<String>()
    val responseStatusLiveData = MutableLiveData<Pair<Boolean, Boolean?>>()
    val responseErrorMessageLiveData = MutableLiveData<String>()

    private val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()

    fun signInUser() {
        firebaseAuth.signInWithEmailAndPassword(emailLiveData.value!!, encrypt(passwordLiveData.value!!)).addOnCompleteListener { authTask ->
            if (authTask.isSuccessful) {
                if(authTask.result != null && authTask.result!!.user != null) {
                    val doc = FirebaseFirestore.getInstance().collection("users").document(authTask.result!!.user!!.uid)
                    doc.get().addOnSuccessListener { documentSnapshot ->
                        val user = documentSnapshot.toObject(User::class.java)
                        if (user!!.student!!) {
                            responseStatusLiveData.postValue(true to true)
                        } else {
                            responseStatusLiveData.postValue(true to false)
                        }
                    }
                }
            } else {
                responseStatusLiveData.postValue(false to null)
                authTask.exception?.let { responseErrorMessageLiveData.postValue(it.localizedMessage) }
            }
        }
    }
}