package com.example.firebaseapp.logIn

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.navigation.fragment.findNavController
import com.example.firebaseapp.R
import com.example.firebaseapp.databinding.FragmentLoginBinding

class LoginFragment : Fragment(), LifecycleObserver {
    private lateinit var viewModel: LoginViewModel
    private var binding: FragmentLoginBinding? = null
    private lateinit var emailObserver: Observer<String>
    private lateinit var passwordObserver: Observer<String>
    private lateinit var statusObserver: Observer<Pair<Boolean, Boolean?>>
    private lateinit var errorObserver: Observer<String>
    private var emailFlag = false
    private var passwordFlag = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        binding?.viewModel = viewModel

        binding!!.buttonNewAccount.setOnClickListener { findNavController().navigate(R.id.action_loginFragment_to_newAccountFragment) }
        binding!!.buttonLogIn.setOnClickListener { onLogInCLick() }
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().setTitle(R.string.welcome)
        lifecycle.addObserver(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun removeObservers() {
        viewModel.emailLiveData.removeObserver(emailObserver)
        viewModel.passwordLiveData.removeObserver(passwordObserver)
        viewModel.responseStatusLiveData.removeObserver(statusObserver)
        viewModel.responseErrorMessageLiveData.removeObserver(errorObserver)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun initObservers() {
        emailObserver = Observer { emailFlag = it != "" }
        passwordObserver = Observer { passwordFlag = it != "" }
        statusObserver = Observer {
            if (it.first) {
                if (it.second!!) {
                    findNavController().navigate(R.id.action_loginFragment_to_studentFragment)
                } else {
                    findNavController().navigate(R.id.action_loginFragment_to_librarianFragment)
                }
                binding!!.emailEditText.text.clear()
                binding!!.passwordEditText.text.clear()
                binding!!.passwordEditText.clearFocus()
            }
        }
        errorObserver = Observer { Toast.makeText(requireActivity(), viewModel.responseErrorMessageLiveData.value!!, Toast.LENGTH_SHORT).show() }

        viewModel.emailLiveData.observe(this, emailObserver)
        viewModel.passwordLiveData.observe(this, passwordObserver)
        viewModel.responseStatusLiveData.observe(this, statusObserver)
        viewModel.responseErrorMessageLiveData.observe(this, errorObserver)
    }

    private fun onLogInCLick() {
        if (emailFlag && passwordFlag) {
            viewModel.signInUser()
        } else {
            Toast.makeText(requireActivity(), "All fields are mandatory!", Toast.LENGTH_SHORT).show()
        }
    }
}