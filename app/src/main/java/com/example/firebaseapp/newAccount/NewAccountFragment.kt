package com.example.firebaseapp.newAccount

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.navigation.fragment.findNavController
import com.example.firebaseapp.R
import com.example.firebaseapp.databinding.FragmentNewAccountBinding

class NewAccountFragment : Fragment(), LifecycleObserver {
    private lateinit var viewModel: NewAccountViewModel
    private var binding: FragmentNewAccountBinding? = null
    private lateinit var userNameObserver: Observer<String>
    private lateinit var emailObserver: Observer<String>
    private lateinit var passwordObserver: Observer<String>
    private lateinit var statusObserver: Observer<Boolean>
    private lateinit var errorObserver: Observer<String>
    private var userNameFlag = false
    private var emailFlag = false
    private var passwordFlag = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(NewAccountViewModel::class.java)

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_new_account, container, false)
        binding?.viewModel = viewModel

        binding!!.buttonCreateAccount.setOnClickListener { onNewAccountClick() }
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().setTitle(R.string.New_account)
        lifecycle.addObserver(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun removeObservers() {
        viewModel.userNameLiveData.removeObserver(userNameObserver)
        viewModel.emailLiveData.removeObserver(emailObserver)
        viewModel.passwordLiveData.removeObserver(passwordObserver)
        viewModel.responseStatusLiveData.removeObserver(statusObserver)
        viewModel.responseErrorMessageLiveData.removeObserver(errorObserver)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun initObservers() {
        userNameObserver = Observer { userNameFlag = it != "" }
        emailObserver = Observer { emailFlag = it != "" }
        passwordObserver = Observer { passwordFlag = it != "" }
        statusObserver = Observer {
            if (it) {
                Toast.makeText(requireActivity(), "Account successfully created!", Toast.LENGTH_SHORT).show()
                findNavController().popBackStack(R.id.loginFragment, false)
            }
        }
        errorObserver = Observer { Toast.makeText(requireActivity(), viewModel.responseErrorMessageLiveData.value!!, Toast.LENGTH_SHORT).show() }

        viewModel.userNameLiveData.observe(this, userNameObserver)
        viewModel.emailLiveData.observe(this, emailObserver)
        viewModel.passwordLiveData.observe(this, passwordObserver)
        viewModel.responseStatusLiveData.observe(this, statusObserver)
        viewModel.responseErrorMessageLiveData.observe(this, errorObserver)
    }

    private fun onNewAccountClick() {
        if (userNameFlag && emailFlag && passwordFlag) {
            viewModel.addNewUser()
        } else {
            Toast.makeText(requireActivity(), "All fields are mandatory!", Toast.LENGTH_SHORT).show()
        }
    }
}