package com.example.firebaseapp.newAccount

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.firebaseapp.dataClasses.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

fun encrypt(pass: String): String {
    var newPass = ""
    pass.forEach { newPass += it + it.code % 10 }
    return newPass
}

class NewAccountViewModel: ViewModel() {
    val userNameLiveData = MutableLiveData<String>()
    val emailLiveData = MutableLiveData<String>()
    val passwordLiveData = MutableLiveData<String>()
    val isStudentLiveData = MutableLiveData(false)
    val responseStatusLiveData = MutableLiveData<Boolean>()
    val responseErrorMessageLiveData = MutableLiveData<String>()

    private val firestore: FirebaseFirestore = FirebaseFirestore.getInstance()
    private val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()

    fun addNewUser() {
        firestore.collection("users").whereEqualTo("username", userNameLiveData.value!!.trim())
            .get().addOnSuccessListener { querySnapshot ->
                if (querySnapshot.isEmpty) {
                    firestore.collection("users").whereEqualTo("email", emailLiveData.value!!.trim())
                        .get().addOnSuccessListener { query ->
                            if (query.isEmpty) {
                                val encryptedPassword = encrypt(passwordLiveData.value!!.trim())
                                firebaseAuth.createUserWithEmailAndPassword(emailLiveData.value!!.trim(), encryptedPassword).addOnCompleteListener { authTask ->
                                    if (authTask.isSuccessful) {
                                        if (authTask.result != null && authTask.result!!.user != null) {
                                            val user =
                                                User(
                                                    username = userNameLiveData.value!!.trim(),
                                                    email = emailLiveData.value!!.trim(),
                                                    password = encryptedPassword,
                                                    student = isStudentLiveData.value!!,
                                                    books = if (isStudentLiveData.value!!) ArrayList() else null
                                                )
                                            val userId = authTask.result!!.user!!.uid

                                            firestore.collection("users").document(userId).set(user)
                                                .addOnCompleteListener { collectionTask ->
                                                    if (collectionTask.isSuccessful) {
                                                        responseStatusLiveData.postValue(true)
                                                    } else {
                                                        responseStatusLiveData.postValue(false)
                                                        collectionTask.exception?.let { responseErrorMessageLiveData.postValue(it.localizedMessage) }
                                                    }
                                                }
                                        }
                                    } else {
                                        responseStatusLiveData.postValue(false)
                                        authTask.exception?.let { responseErrorMessageLiveData.postValue(it.localizedMessage) }
                                    }
                                }
                            } else {
                                responseStatusLiveData.postValue(false)
                                responseErrorMessageLiveData.postValue("An user with this email address already exists!")
                            }
                        }
                } else {
                    responseStatusLiveData.postValue(false)
                    responseErrorMessageLiveData.postValue("An user with this username already exists!")
                }
            }
    }
}