package com.example.firebaseapp.librarian

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.firebaseapp.R
import com.google.android.material.bottomnavigation.BottomNavigationView

class LibrarianFragment : Fragment() {
    private var bottomNavigationView: BottomNavigationView? = null
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_librarian, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().setTitle(R.string.librarian)

        bottomNavigationView = view.findViewById(R.id.bottomNavigationView)
        navController = requireActivity().findNavController(R.id.fragment)

        bottomNavigationView?.setupWithNavController(navController)
   }

    override fun onDestroyView() {
        super.onDestroyView()
        bottomNavigationView = null
    }
}