package com.example.firebaseapp.student

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.firebaseapp.R
import com.google.android.material.bottomnavigation.BottomNavigationView

class StudentFragment : Fragment() {
    private var bottomNavigationView: BottomNavigationView? = null
    private lateinit var navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_student, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().setTitle(R.string.student)

        bottomNavigationView = view.findViewById(R.id.bottomNavigationView)
        navController = requireActivity().findNavController(R.id.fragment2)

        bottomNavigationView?.setupWithNavController(navController)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        bottomNavigationView = null
    }
}