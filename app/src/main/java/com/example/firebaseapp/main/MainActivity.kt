package com.example.firebaseapp.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.example.firebaseapp.R
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onBackPressed() {
        val navController = findNavController(R.id.fragment3)
        when(navController.currentDestination?.id) {
            R.id.librarianFragment -> {
                val navControllerLibrarian = findNavController(R.id.fragment)
                when(navControllerLibrarian.currentDestination?.id) {
                    R.id.addBookFragment -> {
                        navController.popBackStack(R.id.loginFragment, false)
                        FirebaseAuth.getInstance().signOut()
                    }
                    else -> {
                        navControllerLibrarian.popBackStack()
                    }
                }
            }
            R.id.studentFragment -> {
                val navControllerStudent = findNavController(R.id.fragment2)
                when(navControllerStudent.currentDestination?.id) {
                    R.id.borrowBookFragment -> {
                        navController.popBackStack(R.id.loginFragment, false)
                        FirebaseAuth.getInstance().signOut()
                    }
                    else -> {
                        navControllerStudent.navigate(R.id.borrowBookFragment)
                    }
                }
            }
            R.id.newAccountFragment -> {
                navController.popBackStack(R.id.loginFragment, false)
            }
            R.id.showCommentsFragment -> {
                navController.navigate(R.id.action_showCommentsFragment_to_studentFragment)
            }
            else -> { super.onBackPressed() }
        }
    }
}