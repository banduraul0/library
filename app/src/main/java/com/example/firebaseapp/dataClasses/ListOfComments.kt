package com.example.firebaseapp.dataClasses

data class ListOfComments(val comments: ArrayList<Comment> = ArrayList())