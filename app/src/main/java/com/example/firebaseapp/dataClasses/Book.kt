package com.example.firebaseapp.dataClasses

data class Book(val title: String? = null,
           val author: String? = null,
           var borrowed: Boolean = false
)