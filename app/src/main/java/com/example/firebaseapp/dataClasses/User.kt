package com.example.firebaseapp.dataClasses

data class User(val username: String? = null,
                val email: String? = null,
                val password: String? = null,
                val student: Boolean? = null,
                val books: ArrayList<Book>? = null
)