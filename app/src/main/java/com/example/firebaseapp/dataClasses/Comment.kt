package com.example.firebaseapp.dataClasses

data class Comment(val username: String? = null,
                   val comment: String? = null
)