package com.example.firebaseapp.borrowBook

import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapp.R

class BorrowBookViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    var borrowBookInfoTextView: TextView = itemView.findViewById(R.id.borrowBookString)
    var borrowBookBtn: Button = itemView.findViewById(R.id.buttonBorrowBook)
}