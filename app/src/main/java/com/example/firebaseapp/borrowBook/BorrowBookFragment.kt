package com.example.firebaseapp.borrowBook

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapp.R
import com.example.firebaseapp.dataClasses.Book
import com.example.firebaseapp.student.StudentFragmentDirections

class BorrowBookFragment : Fragment(), BorrowBookAdapter.BooksListClickListener, LifecycleObserver {
    private var borrowBookRecyclerView: RecyclerView? = null
    private lateinit var viewModel: BorrowBookViewModel
    private lateinit var bookListObserver: Observer<ArrayList<Book>>
    private var adapter: BorrowBookAdapter? = null
    private  var noBooksTextView: TextView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(BorrowBookViewModel::class.java)

        val view = inflater.inflate(R.layout.fragment_borrow_book, container, false)

        borrowBookRecyclerView = view.findViewById(R.id.borrowBook_recyclerview)
        borrowBookRecyclerView?.layoutManager = LinearLayoutManager(requireActivity())
        adapter = BorrowBookAdapter(this)
        borrowBookRecyclerView?.adapter = adapter!!

        noBooksTextView = view.findViewById(R.id.noBooksTextView)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycle.addObserver(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        noBooksTextView = null
        borrowBookRecyclerView = null
        adapter = null
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun removeObservers() {
        viewModel.bookListLiveData.removeObserver(bookListObserver)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun initObservers() {
        bookListObserver = Observer {
            adapter?.setData(it)
            if (it.isEmpty()) {
                noBooksTextView?.visibility = View.VISIBLE
            } else {
                noBooksTextView?.visibility = View.INVISIBLE
            }
        }

        viewModel.bookListLiveData.observe(this, bookListObserver)
    }

    override fun bookItemClicked(book: Book) {
        requireActivity().findNavController(R.id.fragment3).navigate(StudentFragmentDirections.actionStudentFragmentToShowCommentsFragment(book.title, book.author))
    }
}