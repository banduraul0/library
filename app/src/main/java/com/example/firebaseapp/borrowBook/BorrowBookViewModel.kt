package com.example.firebaseapp.borrowBook

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.firebaseapp.dataClasses.Book
import com.google.firebase.firestore.*

class BorrowBookViewModel: ViewModel() {
    private lateinit var firestore: FirebaseFirestore
    val bookListLiveData = MutableLiveData<ArrayList<Book>>(ArrayList())

    private fun registerBookLiveListener() {
        firestore = FirebaseFirestore.getInstance()
        firestore.collection("books").addSnapshotListener { value, error ->
            if (error != null) {
                Log.w("BOOKS_DATA_ERROR", error.localizedMessage!!)
            }
            value?.let {
                val bookList = ArrayList<Book>()
                bookList.addAll(bookListLiveData.value!!)
                for (dc in it.documentChanges) {
                    when (dc.type) {
                        DocumentChange.Type.ADDED -> {
                            val book = dc.document.toObject(Book::class.java)
                            if (!book.borrowed) {
                                bookList.add(book)
                            }
                        }
                        DocumentChange.Type.MODIFIED -> {
                            val book = dc.document.toObject(Book::class.java)
                            if (book.borrowed) {
                                for (index in 0 until bookList.size) {
                                    if (bookList[index].title == book.title) {
                                        bookList.removeAt(index)
                                        break
                                    }
                                }
                            } else {
                                bookList.add(book)
                            }
                        }
                        DocumentChange.Type.REMOVED -> Log.d("BOOK_REMOVED", dc.document.toString())
                    }
                }
                bookListLiveData.postValue(bookList)
            }
        }
    }

    init {
        registerBookLiveListener()
    }
}