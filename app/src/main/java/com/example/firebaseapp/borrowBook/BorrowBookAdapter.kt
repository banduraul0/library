package com.example.firebaseapp.borrowBook

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapp.R
import com.example.firebaseapp.dataClasses.Book
import com.example.firebaseapp.dataClasses.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class BorrowBookAdapter(private val clickListener: BooksListClickListener): RecyclerView.Adapter<BorrowBookViewHolder>() {
    private val books: ArrayList<Book> = ArrayList()

    interface BooksListClickListener {
        fun bookItemClicked(book: Book)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BorrowBookViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.borrowbook_view_holder, parent, false)
        return BorrowBookViewHolder(view)
    }

    override fun getItemCount(): Int {
        return books.size
    }

    override fun onBindViewHolder(holder: BorrowBookViewHolder, position: Int) {
        val name = "${books[position].title} by ${books[position].author}"
        holder.borrowBookInfoTextView.text = name
        holder.borrowBookBtn.setOnClickListener {
            books[position].borrowed = true
            val firestore = FirebaseFirestore.getInstance()
            val userUid = FirebaseAuth.getInstance().currentUser!!.uid
            firestore.collection("users").document(userUid).get().addOnSuccessListener { documentSnapshot ->
                val user = documentSnapshot.toObject(User::class.java)!!
                user.books?.add(books[position])
                firestore.collection("users").document(userUid).update("books", user.books!!).addOnCompleteListener { updateTask ->
                    if (updateTask.isSuccessful) {
                        firestore.collection("books").document(books[position].title!!).update("borrowed", true).addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                Toast.makeText(it.context.applicationContext, "You have borrowed ${name}!", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }
            }
        }
        holder.itemView.setOnClickListener { clickListener.bookItemClicked(books[position]) }
    }

    fun setData(bookList: ArrayList<Book>) {
        books.clear()
        books.addAll(bookList)
        notifyDataSetChanged()
    }
}