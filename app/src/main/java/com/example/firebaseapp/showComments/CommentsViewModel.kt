package com.example.firebaseapp.showComments

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.firebaseapp.dataClasses.Comment
import com.example.firebaseapp.dataClasses.ListOfComments
import com.google.firebase.firestore.FirebaseFirestore

class CommentsViewModel: ViewModel() {
    private lateinit var firestore: FirebaseFirestore
    val commentListLiveData = MutableLiveData<ArrayList<Comment>>(ArrayList())

    private fun getComments() {
        firestore = FirebaseFirestore.getInstance()
        val commentList = ArrayList<Comment>()

        firestore.collection("comments").document(bookTitle).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                task.result?.let { doc ->
                    val listOfComments = doc.toObject(ListOfComments::class.java)
                    listOfComments?.let {
                        commentList.addAll(it.comments)
                        commentListLiveData.postValue(commentList)
                    }
                }
            }
        }
    }

    init {
        getComments()
    }
}