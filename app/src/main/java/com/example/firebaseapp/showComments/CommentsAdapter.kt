package com.example.firebaseapp.showComments

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapp.R
import com.example.firebaseapp.dataClasses.Comment

class CommentsAdapter: RecyclerView.Adapter<CommentsViewHolder>() {
    private val comments: ArrayList<Comment> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.comments_view_holder, parent, false)
        return CommentsViewHolder(view)
    }

    override fun getItemCount(): Int {
        return comments.size
    }

    override fun onBindViewHolder(holder: CommentsViewHolder, position: Int) {
        val text = "${comments[position].username}: ${comments[position].comment}"
        holder.commentInfo.text = text
    }

    fun setData(commentList: ArrayList<Comment>) {
        comments.clear()
        comments.addAll(commentList)
        notifyDataSetChanged()
    }
}