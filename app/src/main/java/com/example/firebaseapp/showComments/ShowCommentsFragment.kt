package com.example.firebaseapp.showComments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapp.R
import com.example.firebaseapp.dataClasses.Comment

lateinit var bookTitle: String

class ShowCommentsFragment : Fragment(), LifecycleObserver {
    private lateinit var viewModel: CommentsViewModel
    private lateinit var commentListObserver: Observer<ArrayList<Comment>>
    private var adapter: CommentsAdapter? = null
    private var noCommentsTextView: TextView? = null
    private var commentsRecyclerView: RecyclerView? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bookTitle = args.bookTitle!!

        viewModel = ViewModelProvider(this).get(CommentsViewModel::class.java)

        val view = inflater.inflate(R.layout.fragment_show_comments, container, false)

        commentsRecyclerView = view.findViewById(R.id.comments_recyclerview)
        commentsRecyclerView?.layoutManager = LinearLayoutManager(requireActivity())
        adapter = CommentsAdapter()
        commentsRecyclerView?.adapter = adapter

        noCommentsTextView = view.findViewById(R.id.noCommentsTextView)

        return view
    }

    val args: ShowCommentsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireActivity().title = getString(R.string.comments, args.bookTitle, args.bookAuthor)

        lifecycle.addObserver(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        commentsRecyclerView = null
        noCommentsTextView = null
        adapter = null
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun initObservers() {
        commentListObserver = Observer {
            adapter?.setData(it)
            if (it.isEmpty()) {
                noCommentsTextView?.visibility = View.VISIBLE
            } else {
                noCommentsTextView?.visibility = View.INVISIBLE
            }
        }

        viewModel.commentListLiveData.observe(this, commentListObserver)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun removeObservers() {
        viewModel.commentListLiveData.removeObserver(commentListObserver)
    }
}