package com.example.firebaseapp.showComments

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.firebaseapp.R

class CommentsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    var commentInfo: TextView = itemView.findViewById(R.id.commentInfo)
}